package cmd

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"os"
	"strings"

	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

type PersonName struct {
	DrugNames []PhrenDrugName `yaml:"phren_person"`
}

type PhrenDrugName struct {
	DrugName      string `yaml:"drug_name"`
	DrugBrandName string `yaml:"drug_brand_name"`
	DrugBankData  Drugtype
	AdverseEvents []string `yaml:"adverse_events"`
}

// Default Constructor function for PersonName
func NewPersonName() *PersonName {
	var TmpNewPersonName PersonName
	FileName := medicineHistoryFileName

	// Open and read file
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Inside Default PersonName Constructor"}).Debug("Moving to Open file in Default PersonName Constructor")
	yamlFile, err := os.Open(FileName) // Open our yamlFile
	if err != nil {
		log.WithFields(log.Fields{"FileName": FileName, "Location": "open err check", "Error": err}).Error("Failed to open medicine history file, check your config file or pass the correct file via flags")
		fmt.Println(color.HiRedString("%+v\n", err))
		return nil
	}

	if yamlFile != nil {
		b, err := os.ReadFile(FileName)
		if err != nil {
			fmt.Print(err)
		}
		yaml.Unmarshal(b, &TmpNewPersonName)
	}
	return &TmpNewPersonName
}

// We want to load a personName from file
func (p *PersonName) LoadPhrenDrugName() {
	fmt.Println("What yaml file would you like load your medication history from?")

	// Get input from user
	FileName := GetFileName(".yaml")

	// Open and read file
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Begin LoadPhrenDrugName"}).Debug("Entering LoadPhrenDrugName")
	FileName = strings.TrimSpace(FileName)
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Trim that ending newline char"}).Debug("Moving to Open file in LoadPhrenDrugName")
	yamlFile, err := os.Open(FileName) // Open our yamlFile
	if err != nil {
		log.WithFields(log.Fields{"FileName": FileName, "Location": "open err check", "Error": err}).Error("Failed to open file")
		fmt.Println(color.HiRedString("%+v\n", err))
		return
	}
	fmt.Println(color.GreenString("Successfully Opened %s\n", FileName))

	if yamlFile != nil {
		b, err := os.ReadFile(FileName)
		if err != nil {
			fmt.Print(err)
		}
		yaml.Unmarshal(b, &p)
	}
}

// Now we need to attach a drug and its corresponding targets to its corresponding PhrenDrugName
func (p *PersonName) AttachDrugBankData() {
	fmt.Println("What XML file would you like to read from?")

	var tmpDrugBankDrug Drugtype
	// Get input from user
	FileName := GetFileName(".xml")

	// Open and read file
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Begin OpenXMLFile"}).Debug("Entering OpenXMLFile")
	FileName = strings.TrimSpace(FileName)
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Trim that ending newline char"}).Debug("Moving to Open file in OpenXMLFile")
	xmlFile, err := os.Open(FileName) // Open our xmlFile
	if err != nil {
		log.WithFields(log.Fields{"FileName": FileName, "Location": "open err check", "Error": err}).Error("Failed to open file")
		fmt.Println(color.HiRedString("%+v\n", err))
		return
	}
	fmt.Println(color.GreenString("Successfully Opened %s\n", FileName))
	if xmlFile != nil {
		b, err := os.ReadFile(FileName)
		if err != nil {
			fmt.Print(err)
		}
		xml.Unmarshal(b, &tmpDrugBankDrug)

		for i, phrenDrug := range p.DrugNames {
			if strings.EqualFold(tmpDrugBankDrug.Name, phrenDrug.DrugName) {
				fmt.Println("The strings are equal (case-insensitive).")
				p.DrugNames[i].DrugBankData = tmpDrugBankDrug
			} else {
				fmt.Println("The strings are not equal (case-insensitive).")
			}
		}
	}
}

// Scan Drug Bank Database and associate individual matching drugs
func (p *PersonName) AttachDrugBankDataFromFullDatabase() {
	fmt.Println("Loading Database")

	FileName := "db/fulldatabase.xml"

	// Open and read file
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Begin OpenXMLFile"}).Debug("Entering OpenXMLFile")
	FileName = strings.TrimSpace(FileName)
	log.WithFields(log.Fields{"FileName": FileName, "Location": "Trim that ending newline char"}).Debug("Moving to Open file in OpenXMLFile")

	xmlFile, err := os.Open(FileName) // Open our xmlFile
	if err != nil {
		log.WithFields(log.Fields{"FileName": FileName, "Location": "open err check", "Error": err}).Error("Failed to open file")
		fmt.Println(color.HiRedString("%+v\n", err))
		return
	}
	fmt.Println(color.GreenString("Successfully Opened %s\n", FileName))

	if xmlFile != nil {
		b, err := os.ReadFile(FileName)
		if err != nil {
			fmt.Print(err)
		}
		xml.Unmarshal(b, &DrugBankDatabase)

		// Range through the drugs we loaded from our medical history
		for i, phrenDrug := range p.DrugNames {
			// Range the drugs in the drug database
			for _, DrugBankDrug := range DrugBankDatabase.Drug {
				// If the names match then copy over the drug database data to our medical history so we can access the drug targets
				if strings.EqualFold(DrugBankDrug.Name, phrenDrug.DrugName) {
					p.DrugNames[i].DrugBankData = DrugBankDrug
				}
			}
		}
	}
}

func (p *PersonName) AnalyzeNewDrug() {
	// Get the name of the new drug from the user
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter name of new drug: ")
	NewDrug, _ := reader.ReadString('\n')
	NewDrug = strings.TrimSpace(NewDrug)

	// Retrieve new drug data from drug database
	// Range the drugs in the drug database
	for _, DrugBankDrug := range DrugBankDatabase.Drug {
		// If the names match then copy over the drug database data to our medical history so we can access the drug targets
		if strings.EqualFold(DrugBankDrug.Name, NewDrug) {
			fmt.Println("The strings are equal (case-insensitive).")
			NewDrugToAnalyze = DrugBankDrug
		}
	}

	// Compare our drugs and the new drug targets
	// Range through the drugs we loaded from our medical history
	for i, phrenDrug := range p.DrugNames {
		log.WithFields(log.Fields{"Medical History Drug Name": phrenDrug.DrugName, "Location": "AnalyzeNewDrug Inside for loop ranging over our medical history drugs"}).Debug("Moving to range Medical History Drug Targets")

		// Range through the targets of the drug we currently have selected from our medical history
		for _, target := range phrenDrug.DrugBankData.Targets.Target {
			log.WithFields(log.Fields{"Medical History Drug Name": phrenDrug.DrugName, "Medical History Target Name": target.Name, "Location": "AnalyzeNewDrug Inside for loop ranging over our medical history drug targets"}).Debug("Moving to range newDrugToAnalyze targets")

			// range through the targets for the new drug we are analyzing
			for _, newDrugToAnalyzeTarget := range NewDrugToAnalyze.Targets.Target {
				log.WithFields(log.Fields{"Medical History Drug Name": phrenDrug.DrugName, "Medical History Target Name": target.Name, "NewDrugToAnalyze Target": newDrugToAnalyzeTarget, "Location": "AnalyzeNewDrug Inside for loop ranging over newDrugToAnalyze targets"}).Debug("Moving to compare ids for both targets")

				if strings.EqualFold(target.ID, newDrugToAnalyzeTarget.ID) {
					// fmt.Println("The strings are equal (case-insensitive).")
					fmt.Printf("%s targets %s which matches %s targeted by %s\n", p.DrugNames[i].DrugName, target.Name, newDrugToAnalyzeTarget.Name, NewDrugToAnalyze.Name)
					color.HiMagenta("Adverse Events Associated with this Target: %s", phrenDrug.AdverseEvents)
				}
			}
		}
	}
}
